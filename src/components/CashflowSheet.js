import React, { PureComponent } from 'react'
import ReactDataSheet from 'react-datasheet';
import _ from 'lodash';
import * as mathjs from 'mathjs';
import './cashflow-sheet.css'

const SheetRenderer = props => {
  const { as: Tag, headerAs: Header, bodyAs: Body, rowAs: Row, cellAs: Cell,
    className, columns } = props
  return (
    <Tag className={className}>
      <Header className='data-header'>
        <Row>
          {columns.map(column => <Cell className='cell' style={{ width: column.width }} key={column.label}>{column.label}</Cell>)}
        </Row>
      </Header>
      <Body className='data-body'>
        {props.children}
      </Body>
    </Tag>
  )
}

const RowRenderer = props => {
  const { as: Tag, className } = props
  return (
    <Tag className={className}>
      {props.children}
    </Tag>
  )
}

const CellRenderer = props => {
  const {
    as: Tag, cell, row, col, columns, attributesRenderer,
    selected, editing, updated, style,
    ...rest
  } = props

  // handle custom attributes
  const attributes = cell.attributes || {}

  // calculate sheet column
  const sheetCols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']
  const sheetCol = sheetCols[col]

  // calculate sheet row
  const sheetRowOffset = 21
  const sheetRow = row + sheetRowOffset

  // set cell reference
  const cellRef = sheetCol.concat(sheetRow)
  attributes.id = cellRef

  return (
    <Tag {...rest} {...attributes}>
      {props.children}
    </Tag>
  )
}

export default class CashflowSheet extends PureComponent {
  constructor(props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleSelectAllChanged = this.handleSelectAllChanged.bind(this)
    this.handleSelectChanged = this.handleSelectChanged.bind(this)
    this.handleCellsChanged = this.handleCellsChanged.bind(this)

    this.sheetRenderer = this.sheetRenderer.bind(this)
    this.rowRenderer = this.rowRenderer.bind(this)
    this.cellRenderer = this.cellRenderer.bind(this)

    this.state = {
      as: 'table',
      columns: [
        { label: 'PROJECT COSTS (in thousands)', width: '12%' },
        { label: 'PRIOR YEARS', width: '8%' },
        { label: '2019', width: '8%' },
        { label: '2020', width: '8%' },
        { label: '2021', width: '8%' },
        { label: '2022', width: '8%' },
        { label: '2023', width: '8%' },
        { label: '2024', width: '8%' },
        { label: 'REMAINING YEARS', width: '8%' },
        { label: 'CBD Total', width: '8%' },
        { label: 'Out of Balance', width: '8%' },
        { label: 'WOA Total', width: '8%' },
      ],
      grid: [
        [{ value: 'Capital Installation (w/o AFUDC)', expr: '', disableEvents: true }, { value: 0, expr: '' }, { value: 19537, expr: '19537' }, { value: 0, expr: '' }, { value: 0, expr: '' }, { value: 0, expr: '' }, { value: 0, expr: '' }, { value: 0, expr: '' }, { value: 0, expr: '' }, { value: 19537, expr: '19537' }, { value: 0, expr: '' }, { value: 19537, expr: '19537' }, ],
        [{ value: 'Capital Removal', disableEvents: true }, { value: 0 }, { value: 3540 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 3540 }, { value: 0 }, { value: 3540 }, ],
        [{ value: 'TOTAL (w/o AFUDC & Ad Val Tax)', disableEvents: true }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, ]
      ],
      selections: [false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    }
  }

  handleSelect(e) {
    this.setState({ as: e.target.value })
  }

  handleSelectAllChanged(selected) {
    const selections = this.state.selections.map(s => selected)
    this.setState({ selections })
  }

  handleSelectChanged(index, selected) {
    const selections = [...this.state.selections]
    selections[index] = selected
    this.setState({ selections })
  }

  handleCellsChanged(changes) {
    const grid = this.state.grid
    const state = _.assign({}, this.state)
    changes.forEach(({ cell, row, col, value }) => {
      this.cellUpdate(grid, cell, row, col, value)
    })

    this.setState({ grid })
  }

  sheetRenderer(props) {
    const { columns, selections } = this.state
    return <SheetRenderer columns={columns} selections={selections} onSelectAllChanged={this.handleSelectAllChanged} as='table' headerAs='thead' bodyAs='tbody' rowAs='tr' cellAs='th' {...props} />
  }

  rowRenderer(props) {
    const { selections } = this.state
    return <RowRenderer as='tr' cellAs='td' selected={selections[props.row]} onSelectChanged={this.handleSelectChanged} className='data-row' {...props} />
  }

  cellRenderer(props) {
    return <CellRenderer as='td' columns={this.state.columns} {...props} />
  }

  // Sheet Formula Functions

  cellUpdate(grid, changeCell, row, col, expr) {
    const scope = _.map(grid, (val) => {
      return isNaN(val.value) ? 0 : parseFloat(val.value)
    })
    console.log(scope)
    const updatedCell = _.assign({}, changeCell, this.computeExpr(changeCell.cellRef, expr, scope))
    grid[row][col] = updatedCell

    _.each(grid, (cell, cellRef) => {
        if(cell.expr && cell.expr.charAt(0) === '=' && cell.expr.indexOf(changeCell.cellRef) > -1 && cellRef !== changeCell.cellRef) {
          console.log(cell)
          grid = this.cellUpdate(grid, cell, cell.expr)
        }
    })

    return grid
  }

  computeExpr(cellRef, expr, scope) {
    let value = null;
    if(expr.charAt(0) !== '=') {
      return {className: '', value: expr, expr: expr};
    } else {
      try {
        value = mathjs.evaluate(expr.substring(1), scope)
      } catch(e) {
        value = null
      }

      if(value !== null && this.validateExp([cellRef], expr)) {
        return {className: 'equation', value, expr}
      } else {
        return {className: 'error', value: 'error', expr: ''}
      }
    }
  }

  validateExp(trailRefs, expr) {
    let valid = true;
    const matches = expr.match(/[A-Z][1-9]+/g) || [];
    matches.map(match => {
      if(trailRefs.indexOf(match) > -1) {
        valid = false
      } else {
        valid = this.validateExp([...trailRefs, match], this.state[match].expr)
      }
    })
    return valid
  }

  render() {
    return (
      <div>
        <ReactDataSheet
          data={this.state.grid}
          className='custom-sheet'
          sheetRenderer={this.sheetRenderer}
          headerRenderer={this.headerRenderer}
          bodyRenderer={this.bodyRenderer}
          rowRenderer={this.rowRenderer}
          cellRenderer={this.cellRenderer}
          onCellsChanged={this.handleCellsChanged}
          valueRenderer={(cell) => cell.value}
        />
      </div>
    )
  }
}
