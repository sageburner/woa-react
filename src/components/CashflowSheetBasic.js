import React from 'react';
import ReactDataSheet from 'react-datasheet';
import _ from 'lodash';
import * as mathjs from 'mathjs';

export default class MathSheet extends React.Component {
  constructor(props) {
    super(props)
    this.onCellsChanged = this.onCellsChanged.bind(this);
    this.state = {
      // Header Row
      'A20': {key: 'A20', value: 'PROJECT COSTS (in thousands)', expr: ''},
      'B20': {key: 'B20', value: 'PRIOR YEARS', expr: ''},
      'C20': {key: 'C20', value: '2019', expr: ''},
      'D20': {key: 'D20', value: '2020', expr: ''},
      'E20': {key: 'E20', value: '2021', expr: ''},
      'F20': {key: 'F20', value: '2022', expr: ''},
      'G20': {key: 'G20', value: '2023', expr: ''},
      'H20': {key: 'H20', value: '2024', expr: ''},
      'I20': {key: 'I20', value: 'REMAINING YEARS', expr: ''},
      'J20': {key: 'J20', value: 'CBD Total', expr: ''},
      'K20': {key: 'K20', value: 'Out of Balance', expr: ''},
      'L20': {key: 'L20', value: 'WOA Total', expr: ''},

      // Capital Installation (w/o AFUDC)
      'A21': {key: 'A21', value: 'Capital Installation (w/o AFUDC)', expr: ''},
      'B21': {key: 'B21', value: '0', expr: '0'},
      'C21': {key: 'C21', value: '19537', expr: '19537'},
      'D21': {key: 'D21', value: '0', expr: '0'},
      'E21': {key: 'E21', value: '0', expr: '0'},
      'F21': {key: 'F21', value: '0', expr: '0'},
      'G21': {key: 'G21', value: '0', expr: '0'},
      'H21': {key: 'H21', value: '0', expr: '0'},
      'I21': {key: 'I21', value: '0', expr: '0'},
      'J21': {key: 'J21', value: '19537', expr: '19537'},
      'K21': {key: 'K21', value: '0', expr: '19537'},
      'L21': {key: 'L21', value: '19537', expr: '0'},

      // Capital Removal
      'A22': {key: 'A22', value: 'Capital Removal', expr: ''},
      'B22': {key: 'B22', value: '0', expr: '0'},
      'C22': {key: 'C22', value: '3540', expr: '3540'},
      'D22': {key: 'D22', value: '0', expr: '0'},
      'E22': {key: 'E22', value: '0', expr: '0'},
      'F22': {key: 'F22', value: '0', expr: '0'},
      'G22': {key: 'G22', value: '0', expr: '0'},
      'H22': {key: 'H22', value: '0', expr: '0'},
      'I22': {key: 'I22', value: '0', expr: '0'},
      'J22': {key: 'J22', value: '3540', expr: '3540'},
      'K22': {key: 'K22', value: '0', expr: '0'},
      'L22': {key: 'L22', value: '3540', expr: '3540'},

      // TOTAL (w/o AFUDC & Ad Val Tax)
      'A23': {key: 'A23', value: 'TOTAL (w/o AFUDC & Ad Val Tax)', expr: ''},
      'B23': {key: 'B23', value: '', expr: '=B21+B22', className:'equation'},
      'C23': {key: 'C23', value: '', expr: '=C21+C22', className:'equation'},
      'D23': {key: 'D23', value: '', expr: '=D21+D22', className:'equation'},
      'E23': {key: 'E23', value: '', expr: '=E21+E22', className:'equation'},
      'F23': {key: 'F23', value: '', expr: '=F21+F22', className:'equation'},
      'G23': {key: 'G23', value: '', expr: '=G21+G22', className:'equation'},
      'H23': {key: 'H23', value: '', expr: '=H21+H22', className:'equation'},
      'I23': {key: 'I23', value: '', expr: '=I21+I22', className:'equation'},
      'J23': {key: 'J23', value: '', expr: '=J21+J22', className:'equation'},
      'K23': {key: 'K23', value: '', expr: '=K21+K22', className:'equation'},
      'L23': {key: 'L23', value: '', expr: '=L21+L22', className:'equation'},
    }
  }


  generateGrid() {
    return [0, 20, 21,22,23].map((row, i) => 
      ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'].map((col, j) => {
        if(i == 0 && j == 0) {
          return {readOnly: true, value: ''}
        }
        if(row === 0) {
          return {readOnly: true, value: col}
        } 
        if(j === 0) {
          return {readOnly: true, value: row}
        }
        return this.state[col + row]
      })
    )
  }

  validateExp(trailKeys, expr) {
    let valid = true;
    const matches = expr.match(/[A-Z][1-9]+/g) || [];
    matches.map(match => {
      if(trailKeys.indexOf(match) > -1) {
        valid = false
      } else {
        valid = this.validateExp([...trailKeys, match], this.state[match].expr)
      }
    })
    return valid
  }

  computeExpr(key, expr, scope) {
    let value = null;
    if(expr.charAt(0) !== '=') {
      return {className: '', value: expr, expr: expr};
    } else {
      try {
        value = mathjs.evaluate(expr.substring(1), scope)
      } catch(e) {
        value = null
      }

      if(value !== null && this.validateExp([key], expr)) {
        return {className: 'equation', value, expr}
      } else {
        return {className: 'error', value: 'error', expr: ''}
      }
    }
  }

  cellUpdate(state, changeCell, expr) {
    const scope = _.mapValues(state, (val) => isNaN(val.value) ? 0 : parseFloat(val.value))
    console.log(scope)
    const updatedCell = _.assign({}, changeCell, this.computeExpr(changeCell.key, expr, scope))
    state[changeCell.key] = updatedCell

    _.each(state, (cell, key) => {
      if(cell.expr.charAt(0) === '=' && cell.expr.indexOf(changeCell.key) > -1 && key !== changeCell.key) {
        state = this.cellUpdate(state, cell, cell.expr)
      }
    })
    return state
  }

  onCellsChanged(changes) {
    const state = _.assign({}, this.state)
    changes.forEach(({cell, value}) => {
      this.cellUpdate(state, cell, value)
    })
    this.setState(state)
  }

  render() {

    return (
      <ReactDataSheet
        data={this.generateGrid()}
        valueRenderer={(cell) => cell.value}
        dataRenderer={(cell) => cell.expr}
        onCellsChanged={this.onCellsChanged}
      />
    )
  }

}
