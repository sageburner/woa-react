import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { CashflowSheetBasic, CashflowSheet } from './components/index';
import 'react-datasheet/lib/react-datasheet.css';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <div className={'header'}>
          <h1>Work Order Authorization</h1>
          <h4>Capital Budget Document</h4>
        </div>
        <div className={'container'} >
          <Tabs>
            <TabList>
              <Tab>Basic</Tab>
              <Tab>Advanced</Tab>
            </TabList>
            <TabPanel>
              <div className={'sheet-container'}>
                <CashflowSheetBasic />
              </div>
            </TabPanel>
            <TabPanel>
              <div className={'sheet-container'}>
                <CashflowSheet />
              </div>
            </TabPanel>
          </Tabs>
        </div>
        <div className={'footer-container'}>
          <div className={'footer'} >
            Check out the GitHub project at <a href='https://github.com/nadbm/react-datasheet'>react-datasheet</a>
          </div>
        </div>
      </div>
    )
  }
}